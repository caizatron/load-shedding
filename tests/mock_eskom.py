#      A python library for getting Load Shedding schedules.
#      Copyright (C) 2021  Werner Pieterson
#
#      This program is free software: you can redistribute it and/or modify
#      it under the terms of the GNU General Public License as published by
#      the Free Software Foundation, either version 3 of the License, or
#      (at your option) any later version.
#
#      This program is distributed in the hope that it will be useful,
#      but WITHOUT ANY WARRANTY; without even the implied warranty of
#      MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#      GNU General Public License for more details.
#
#      You should have received a copy of the GNU General Public License
#      along with this program.  If not, see <https://www.gnu.org/licenses/>.

from typing import List

from load_shedding.providers.eskom import Eskom, AreaInfo, Province, Stage, Suburb, Municipality


class MockEskom(Eskom):
    def find_suburbs(self, search_text: str, max_results: int = 300) -> List[Suburb]:
        return [
            Suburb(**{
                'Id': 1058852,
                'MunicipalityName': 'City of Cape Town',
                'Name': 'Milnerton',
                'ProvinceName': 'Western Cape',
                'Total': 704
            }),
            Suburb(**{
                'Id': 1058853,
                'MunicipalityName': 'City of Cape Town',
                'Name': 'Milnerton Golf Course',
                'ProvinceName': 'Western Cape',
                'Total': 0
            }),
            Suburb(**{
                'Id': 1058854,
                'MunicipalityName': 'City of Cape Town',
                'Name': 'Milnerton Outlying',
                'ProvinceName': 'Western Cape',
                'Total': 0
            }),
            Suburb(**{
                'Id': 1058855,
                'MunicipalityName': 'City of Cape Town',
                'Name': 'Milnerton Ridge',
                'ProvinceName': 'Western Cape',
                'Total': 704
            }),
            Suburb(**{
                'Id': 1058856,
                'MunicipalityName': 'City of Cape Town',
                'Name': 'Milnerton SP',
                'ProvinceName': 'Western Cape',
                'Total': 2816
            }),
            Suburb(**{
                'Id': 1069144,
                'MunicipalityName': 'City of Cape Town',
                'Name': 'Milnerton SP 1',
                'ProvinceName': 'Western Cape',
                'Total': 3520
            }),
            Suburb(**{
                'Id': 1069145,
                'MunicipalityName': 'City of Cape Town',
                'Name': 'Milnerton SP 2',
                'ProvinceName': 'Western Cape',
                'Total': 700
            })
        ]

    def get_municipalities(self, province: Province) -> List[Municipality]:
        return [
            Municipality({'Disabled': False, 'Group': None, 'Selected': False, 'Text': 'Beaufort West', 'Value': '336'}),
            Municipality({'Disabled': False, 'Group': None, 'Selected': False, 'Text': 'Bergrivier', 'Value': '337'}),
            Municipality({'Disabled': False, 'Group': None, 'Selected': False, 'Text': 'Bitou', 'Value': '338'}),
            Municipality({'Disabled': False, 'Group': None, 'Selected': False, 'Text': 'Breede Valley', 'Value': '339'}),
            Municipality({'Disabled': False, 'Group': None, 'Selected': False, 'Text': 'Cape Agulhas', 'Value': '340'}),
            Municipality({'Disabled': False, 'Group': None, 'Selected': False, 'Text': 'Cederberg', 'Value': '341'}),
            Municipality({'Disabled': False, 'Group': None, 'Selected': False, 'Text': 'City of Cape Town', 'Value': '342'}),
            Municipality({'Disabled': False, 'Group': None, 'Selected': False, 'Text': 'Drakenstein', 'Value': '343'}),
            Municipality({'Disabled': False, 'Group': None, 'Selected': False, 'Text': 'George', 'Value': '344'}),
            Municipality({'Disabled': False, 'Group': None, 'Selected': False, 'Text': 'Hessequa', 'Value': '345'}),
            Municipality({'Disabled': False, 'Group': None, 'Selected': False, 'Text': 'Kannaland', 'Value': '346'}),
            Municipality({'Disabled': False, 'Group': None, 'Selected': False, 'Text': 'Knysna', 'Value': '347'}),
            Municipality({'Disabled': False, 'Group': None, 'Selected': False, 'Text': 'Laingsburg', 'Value': '348'}),
            Municipality({'Disabled': False, 'Group': None, 'Selected': False, 'Text': 'Langeberg', 'Value': '349'}),
            Municipality({'Disabled': False, 'Group': None, 'Selected': False, 'Text': 'Matzikama', 'Value': '350'}),
            Municipality({'Disabled': False, 'Group': None, 'Selected': False, 'Text': 'Mossel Bay', 'Value': '351'}),
            Municipality({'Disabled': False, 'Group': None, 'Selected': False, 'Text': 'Oudtshoorn', 'Value': '352'}),
            Municipality({'Disabled': False, 'Group': None, 'Selected': False, 'Text': 'Overstrand', 'Value': '353'}),
            Municipality({'Disabled': False, 'Group': None, 'Selected': False, 'Text': 'Prince Albert', 'Value': '354'}),
            Municipality({'Disabled': False, 'Group': None, 'Selected': False, 'Text': 'Saldanha Bay', 'Value': '355'}),
            Municipality({'Disabled': False, 'Group': None, 'Selected': False, 'Text': 'Stellenbosch', 'Value': '356'}),
            Municipality({'Disabled': False, 'Group': None, 'Selected': False, 'Text': 'Swartland', 'Value': '357'}),
            Municipality({'Disabled': False, 'Group': None, 'Selected': False, 'Text': 'Swellendam', 'Value': '358'}),
            Municipality({'Disabled': False, 'Group': None, 'Selected': False, 'Text': 'Theewaterskloof', 'Value': '359'}),
            Municipality({'Disabled': False, 'Group': None, 'Selected': False, 'Text': 'Witzenberg', 'Value': '360'})
        ]

    def get_schedule(self, province: Province, suburb: Suburb, stage: Stage) -> List[List]:
        return [
            ["2022-06-26T12:00:00+00:00", "2022-06-26T14:30:00+00:00"], ["2022-06-26T20:00:00+00:00", "2022-06-26T22:30:00+00:00"],
            ["2022-06-27T20:00:00+00:00", "2022-06-27T22:30:00+00:00"], ["2022-06-28T04:00:00+00:00", "2022-06-28T06:30:00+00:00"],
            ["2022-06-29T02:00:00+00:00", "2022-06-29T04:30:00+00:00"], ["2022-06-29T10:00:00+00:00", "2022-06-29T12:30:00+00:00"],
            ["2022-06-30T10:00:00+00:00", "2022-06-30T12:30:00+00:00"], ["2022-06-30T18:00:00+00:00", "2022-06-30T20:30:00+00:00"],
            ["2022-07-01T16:00:00+00:00", "2022-07-01T18:30:00+00:00"], ["2022-07-02T00:00:00+00:00", "2022-07-02T02:30:00+00:00"],
            ["2022-07-03T00:00:00+00:00", "2022-07-03T02:30:00+00:00"], ["2022-07-03T08:00:00+00:00", "2022-07-03T10:30:00+00:00"],
            ["2022-07-04T08:00:00+00:00", "2022-07-04T10:30:00+00:00"], ["2022-07-04T16:00:00+00:00", "2022-07-04T18:30:00+00:00"],
            ["2022-07-05T14:00:00+00:00", "2022-07-05T16:30:00+00:00"], ["2022-07-05T22:00:00+00:00", "2022-07-06T00:30:00+00:00"],
            ["2022-07-06T22:00:00+00:00", "2022-07-07T00:30:00+00:00"], ["2022-07-07T06:00:00+00:00", "2022-07-07T08:30:00+00:00"],
            ["2022-07-08T06:00:00+00:00", "2022-07-08T08:30:00+00:00"], ["2022-07-08T14:00:00+00:00", "2022-07-08T16:30:00+00:00"],
            ["2022-07-09T12:00:00+00:00", "2022-07-09T14:30:00+00:00"], ["2022-07-09T20:00:00+00:00", "2022-07-09T22:30:00+00:00"],
            ["2022-07-10T20:00:00+00:00", "2022-07-10T22:30:00+00:00"], ["2022-07-11T04:00:00+00:00", "2022-07-11T06:30:00+00:00"],
            ["2022-07-12T04:00:00+00:00", "2022-07-12T06:30:00+00:00"], ["2022-07-12T12:00:00+00:00", "2022-07-12T14:30:00+00:00"],
            ["2022-07-13T10:00:00+00:00", "2022-07-13T12:30:00+00:00"], ["2022-07-13T18:00:00+00:00", "2022-07-13T20:30:00+00:00"],
            ["2022-07-14T18:00:00+00:00", "2022-07-14T20:30:00+00:00"], ["2022-07-15T02:00:00+00:00", "2022-07-15T04:30:00+00:00"],
            ["2022-07-16T02:00:00+00:00", "2022-07-16T04:30:00+00:00"], ["2022-07-16T10:00:00+00:00", "2022-07-16T12:30:00+00:00"],
            ["2022-07-17T08:00:00+00:00", "2022-07-17T10:30:00+00:00"], ["2022-07-17T16:00:00+00:00", "2022-07-17T18:30:00+00:00"],
            ["2022-07-18T16:00:00+00:00", "2022-07-18T18:30:00+00:00"], ["2022-07-19T00:00:00+00:00", "2022-07-19T02:30:00+00:00"],
            ["2022-07-20T00:00:00+00:00", "2022-07-20T02:30:00+00:00"], ["2022-07-20T08:00:00+00:00", "2022-07-20T10:30:00+00:00"],
            ["2022-07-21T06:00:00+00:00", "2022-07-21T08:30:00+00:00"], ["2022-07-21T14:00:00+00:00", "2022-07-21T16:30:00+00:00"],
            ["2022-07-22T14:00:00+00:00", "2022-07-22T16:30:00+00:00"], ["2022-07-22T22:00:00+00:00", "2022-07-23T00:30:00+00:00"],
            ["2022-07-23T22:00:00+00:00", "2022-07-24T00:30:00+00:00"], ["2022-07-24T06:00:00+00:00", "2022-07-24T08:30:00+00:00"],
        ]

    def get_schedule_area_info(self, suburb_id: int) -> AreaInfo:
        return AreaInfo(**{
            'Province': {'Id': 9, 'Name': 'Western Cape'},
            'Municipality': {'Id': 342, 'Name': 'City of Cape Town'},
            'Suburb': {'Id': 1058852, 'Name': 'Milnerton'},
            'Period': ['11-11-2021', '08-12-2021']
        })

    def get_stage(self) -> Stage:
        return Stage.NO_LOAD_SHEDDING
